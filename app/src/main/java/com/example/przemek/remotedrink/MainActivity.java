package com.example.przemek.remotedrink;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Icon;
import android.net.sip.SipAudioCall;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.imageButton1)
    ImageButton imageButton1;
    @BindView(R.id.imageButton2)
    ImageButton imageButton2;
    @BindView(R.id.imageButton3)
    ImageButton imageButton3;
    @BindView(R.id.imageButton4)
    ImageButton imageButton4;
    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        imageButton2.setImageResource(R.drawable.alcoholic_drink);
//        imageButton1.setOnClickListener(MainActivity.this);

        imageButton3.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                alertDialogIcons();
                return true;
            }
        });

        final int[] icons = new int[]{
                R.drawable.alcoholic_drink,
                R.drawable.beer_1,
                R.drawable.beer,
                R.drawable.champagne,
                R.drawable.cocktail,
                R.drawable.ice_tea,
                R.drawable.martini,
                R.drawable.tequila,
                R.drawable.vodka,
                R.drawable.whiskey,
                R.drawable.wine_bottle};

        final String[] a = {"alcoholic_drink",
                "beer_1", "beer", "champagne",
                "cocktail", "ice_tea","martini",
                "tequila", "vodka", "whiskey",
                "wine_bottle"};


        imageButton1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                alertDialog.setTitle("Choise icon");
                alertDialog.setItems(a, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        imageButton1.setImageResource(icons[i]);
                    }
                });
                alertDialog.show();
                return true;
            }
        });


    }


    private void alertDialogIcons() {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        final String[] icons = new String[]{
                "R.drawable.alcoholic_drink",
                "R.drawable.beer_1",
                "R.drawable.beer",
                "R.drawable.champagne",
                "R.drawable.cocktail",
                "R.drawable.ice_tea",
                "R.drawable.martini",
                "R.drawable.tequila",
                "R.drawable.vodka",
                "R.drawable.whiskey",
                "R.drawable.wine_bottle"};
        final int[] icon = new int[]{
                R.drawable.alcoholic_drink,
                R.drawable.beer_1,
                R.drawable.beer,
                R.drawable.champagne,
                R.drawable.cocktail,
                R.drawable.ice_tea,
                R.drawable.martini,
                R.drawable.tequila,
                R.drawable.vodka,
                R.drawable.whiskey,
                R.drawable.wine_bottle};

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(icons);
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alertDialog.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                arrayAdapter.getItem(i);
                AlertDialog.Builder newAlertDialog = new AlertDialog.Builder(MainActivity.this);
                newAlertDialog.setMessage(arrayAdapter.getItem(i));

                newAlertDialog.setTitle("Your selected item");
                newAlertDialog.setItems(icons, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        imageButton3.setImageResource(icon[i]);
                    }
                });
                newAlertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                    }
                });
                newAlertDialog.show();
            }
        });
        alertDialog.show();
    }

}
